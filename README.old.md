# react-setup
1) Setup react with umi

Install Yarn: 
>> npm install --global yarn
>> yarn create umi

Install Presets:
>> yarn add @umijs/preset-react -D

Create Routes:
>> yarn global add npx

Create /products Route:
>> npx umi g page products --typescript

2) Create New Project
>> yarn create react-app react-setup

Install antd:
>> yarn add antd

Jest in package.json: test Ant Design application

"jest": {
    "transformIgnorePatterns": [
      "/node_modules/(?!antd|@ant-design|rc-.+?|@babel/runtime).+(js|jsx)$"
    ]
}

3) Modify Webpack config
>> yarn add @craco/craco